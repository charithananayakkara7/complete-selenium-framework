# Complete Selenium Framework

[![Eranda Kodagoda](http://alexanderrem.weebly.com/uploads/7/2/5/6/72566533/linkedin_orig.png)](https://www.linkedin.com/in/erandakodagoda/)

[![Build Status](https://camo.githubusercontent.com/80a3fec8ea098e04112a6ae287b515a104f55b18/68747470733a2f2f6170702e776572636b65722e636f6d2f7374617475732f39653062633534633433663438643232306165633638346666666232623131302f732f6d6173746572)]()

This framework built to integrate with any web application which needs an end to end automation testing. This framework is using fully generic methods. Since this is built with BDD (Behaviour Driven Development) It's easy to understand by anyone who doesn't have a much knowledge about development.
Below are the technologies which have used to implement this framework.

  - Selenium Web Driver
  - Chrome Driver , Gheko Driver
  - Test NG
  - Gherkin Cucumber
  
# New Features!

  - This framework have used Page Object Model
  - Generic methods to handle common actions
  - Cucumber for BDD


You can also:
  - Cross Browser functionality using Browserstack 
  - Implement with Appium for mobile application testings by doing simple changes.

### Tech

Selenium Compelete Framework uses a number of open source projects to work properly:

* [Selenium Web Driver] - To interact with selenium and the application.
* [Gherkin Cucumber] - Behaviour Driven Development.
* [Chrome Driver] - Chrome Browser.
* [Gheko Driver] - Firefox Browser.

And of course This framework itself is open source with a [public repository][dill]
 on GitLab.

### Installation

This Framework requires [Selenium Web Driver](https://selenium.dev/) v3+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd complete-selenium-framework
$ mvn install
```

### Development

Want to contribute? Great!

This framework uses Java + Meven for fast developing.
Make a change in your file and instantaneously see your updates!

### Todos

 - Write MORE Tests
 - Implement BDD 
 - Implement Reporting

License
----

MIT


**Free Software, Hell Yeah!**
